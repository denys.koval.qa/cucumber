package utils;

public enum HttpMethod {
    GET,
    PUT,
    PATCH,
    POST,
    DELETE
}