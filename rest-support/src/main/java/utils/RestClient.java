package utils;

import com.google.gson.Gson;

import configuration.ConfigurationReader;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class RestClient {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestClient.class);
    private final String BASE_URL =
            ConfigurationReader.getPropertiesFromFile("rest_templates.properties").getProperty("base.url");

    private String authorizationValue;
    private static final String AUTHORIZATION = "Authorization";
    private RequestSpecification client;


    private RestClient() {
        RestAssured.defaultParser = Parser.JSON;
        updateHeaders();
    }

    private void updateHeaders() {
        try {
            authorizationValue = getAuthorizationToken();
        } catch (Exception tee) {
            LOGGER.error(tee.getMessage());
        }
    }


    public static RestClient getInstance() {
        return RestClientWrapper.instance;
    }

    public RequestSpecification createRequestSpecification() {
        client = RestAssured.given().headers(getHeaders());
        return client;
    }

    public <T> Response sendRequest(HttpMethod httpMethod, String template, T entity) {
        Response response;
        createRequestSpecification();
        String templatePath = BASE_URL + template;

        response = sendRequestForHttpMethod(httpMethod,
                client
                        .contentType(ContentType.JSON)
                        .body(new Gson().toJson(entity)),
                templatePath);
        return response;
    }

    private Headers getHeaders() {
        return new Headers(new Header(AUTHORIZATION, authorizationValue));
    }

    private Response sendRequestForHttpMethod(HttpMethod httpMethod, RequestSpecification requestSpecification,
                                              String url) {
        Response response = null;
        switch (httpMethod) {
            case GET:
                response = requestSpecification.get(url);
                break;
            case POST:
                response = requestSpecification.post(url);
                break;
            case PUT:
                response = requestSpecification.put(url);
                break;
            case DELETE:
                response = requestSpecification.delete(url);
                break;

            case PATCH:
                response = requestSpecification.patch(url);
                break;
        }
        return response;
    }

    public static String getAuthorizationToken() {
        /* logic for get token*/
        return StringUtils.EMPTY;
    }

    private static final class RestClientWrapper {
        static RestClient instance = new RestClient();

        private RestClientWrapper() {
        }
    }
}
