package rest_definitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import response.DTOResponse;
import utils.GenericRestUtils;
import utils.HttpMethod;
import utils.RestClient;


import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;


public class SearchStepDefinitions {

    private DTOResponse[] dtoResponse;
    private List<DTOResponse> filteredBrands;
    private Response response;
    private static final Logger LOG = LogManager.getLogger(SearchStepDefinitions.class);


    @Given("User calls endpoint {}")
    public void UserCallsEndpoint(String endpoint) {
        response =
                RestClient.getInstance()
                        .sendRequest(HttpMethod.GET, endpoint, false);

        dtoResponse =
                GenericRestUtils.getResponseAsJsonObject(response, DTOResponse[].class);
    }

    @When("User filtered brand {}")
    public void UserFilteredBrands(String brand) {
        filteredBrands = Arrays.stream(dtoResponse).filter(eachBrand ->
                eachBrand.getBrand().equals(brand)).collect(Collectors.toList());
    }

    @Then("User checks that count of filtered brands is {int}")
    public void UserChecksCountOfFilteredBrands(int count) {
        assertThat(filteredBrands.size()).isEqualTo(count);
        LOG.info("Successfully Validated count of filtered brands");

    }
}
