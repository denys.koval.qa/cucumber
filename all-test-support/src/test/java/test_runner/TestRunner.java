package test_runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.CucumberOptions.SnippetType;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		plugin = {
				"pretty:target/cucumber/cucumber.txt",
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:",
				"json:target/cucumber/cucumber.json"
		},
		features = {"src/test/resources/features"},
		glue = {"rest_definitions"},
		monochrome = true,
		snippets = SnippetType.CAMELCASE,
		tags = "@testProject",
		publish = true
)
public class TestRunner {
}
