# CUCUMBER-STARTER-NEW project

## **Overview:**
After analyzing previous framework I desided to re-write it from scratch
What was changed :
1. Was created two modules to split logic :
    - all-test-support module include TestRunner, features files and common properties files
    - rest-support modules include logic of rest part (Client,Configuration)
    - if the test task included some UI logic, I will create the third module (ui-support) that include logic of UI configuration
2. Instead of SerenityRest used RestAssured to make possible work with DTO
    - in rest-support created folder response to deserialize response
3. Delete gradle configuration (task must me rewrite using maven)
4. Delete empty class CarsApi as unnecessary
5. Delete serenity.conf (we do not use UI logic in task)
6. Add gitlab-ci.yml file to make possible run tests and generate report on images

## **Start implement new .feature file:**
1. Go to all-test-support/src/test/resources/features and create a new .feature file
2. Create a new stepsMethods if needed in all-test-support/src/main/java/rest_definitions/SearchStepDefinitions.java
3. In TestRunner.java class add additional CucumberOptions if needed
4. Create docker image using Dockerfile or use existing one [You will use that image in gitlab-ci.yml file]
5. Run tests using command below and to go all-test-support/target/site/allure-maven-plugin and open index.html report
6. Push your changes to GitLab [https://gitlab.com/denys.koval.qa/cucumber]
7. Pipeline will start with two stages [run tests and generate report]
8. See report Artifact tab

### **Some of the key features of this framework:**

1. RestAssured to make possible work with DTO.
2. It generates Allure report.
3. Test execution can be triggered form command line.
4. Easy integration to CI/CD pipeline.

## **Required Setup :**

- [Java](https://www.guru99.com/install-java.html) should be installed and configured.
- [Maven](https://mkyong.com/maven/how-to-install-maven-in-windows/) should be installed and configured.
- [Docker](https://www.docker.com/) should be installed and configured.

## **Running Test:**

Open the command prompt and navigate to the folder in which pom.xml file is present.
Run the below Maven command.

    mvn clean test allure:report


Once the execution completes report & log will be generated in below folder.

**Report:** 		*all-test-support/target/site/allure-maven-plugin*<br>
